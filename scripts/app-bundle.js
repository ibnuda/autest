define('web-api',["require", "exports"], function (require, exports) {
    "use strict";
    var latency = 200;
    var id = 0;
    function getId() {
        return ++id;
    }
    var contacts = [
        {
            id: getId(),
            firstName: 'John',
            lastName: 'Tolkien',
            email: 'tolkien@inklings.com',
            phoneNumber: '867-5309'
        },
        {
            id: getId(),
            firstName: 'Clive',
            lastName: 'Lewis',
            email: 'lewis@inklings.com',
            phoneNumber: '867-5309'
        },
        {
            id: getId(),
            firstName: 'Owen',
            lastName: 'Barfield',
            email: 'barfield@inklings.com',
            phoneNumber: '867-5309'
        },
        {
            id: getId(),
            firstName: 'Charles',
            lastName: 'Williams',
            email: 'williams@inklings.com',
            phoneNumber: '867-5309'
        },
        {
            id: getId(),
            firstName: 'Roger',
            lastName: 'Green',
            email: 'green@inklings.com',
            phoneNumber: '867-5309'
        }
    ];
    var WebAPI = (function () {
        function WebAPI() {
            this.isRequesting = false;
        }
        WebAPI.prototype.getContactList = function () {
            var _this = this;
            this.isRequesting = true;
            return new Promise(function (resolve) {
                setTimeout(function () {
                    var results = contacts.map(function (x) {
                        return {
                            id: x.id,
                            firstName: x.firstName,
                            lastName: x.lastName,
                            email: x.email
                        };
                    });
                    resolve(results);
                    _this.isRequesting = false;
                }, latency);
            });
        };
        WebAPI.prototype.getContactDetails = function (id) {
            var _this = this;
            this.isRequesting = true;
            return new Promise(function (resolve) {
                setTimeout(function () {
                    var found = contacts.filter(function (x) { return x.id == id; })[0];
                    resolve(JSON.parse(JSON.stringify(found)));
                    _this.isRequesting = false;
                }, latency);
            });
        };
        WebAPI.prototype.saveContact = function (contact) {
            var _this = this;
            this.isRequesting = true;
            return new Promise(function (resolve) {
                setTimeout(function () {
                    var instance = JSON.parse(JSON.stringify(contact));
                    var found = contacts.filter(function (x) { return x.id == contact.id; })[0];
                    if (found) {
                        var index = contacts.indexOf(found);
                        contacts[index] = instance;
                    }
                    else {
                        instance.id = getId();
                        contacts.push(instance);
                    }
                    _this.isRequesting = false;
                    resolve(instance);
                }, latency);
            });
        };
        return WebAPI;
    }());
    exports.WebAPI = WebAPI;
});

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
define('netw/chin-service',["require", "exports", 'aurelia-http-client', 'aurelia-framework', 'whatwg-fetch'], function (require, exports, aurelia_http_client_1, aurelia_framework_1) {
    "use strict";
    var ChinService = (function () {
        function ChinService(http) {
            this.http = http.configure(function (config) {
                config
                    .withBaseUrl('http://a.4cdn.org/')
                    .withInterceptor({
                    request: function (req) {
                        console.log("requesting method: " + req.method + ", url: " + req.url);
                        return req;
                    },
                    response: function (resp) {
                        console.log("response status: " + resp.statusCode + ", content : " + resp.content);
                        return resp;
                    }
                });
            });
        }
        ChinService.prototype.getBoards = function () {
            console.log("what the fuck?!");
            return this.http
                .get("boards.json")
                .then(function (resp) {
                if (resp.headers.get('Content-Type') === 'application/json') {
                    console.log("it looks like this is a json file.");
                    return resp.response;
                }
                console.log("it looks like this is not a json file.");
                return resp.responseType;
            });
        };
        ChinService = __decorate([
            aurelia_framework_1.inject(aurelia_http_client_1.HttpClient), 
            __metadata('design:paramtypes', [aurelia_http_client_1.HttpClient])
        ], ChinService);
        return ChinService;
    }());
    exports.ChinService = ChinService;
});

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
define('app',["require", "exports", 'aurelia-framework', './web-api', './netw/chin-service'], function (require, exports, aurelia_framework_1, web_api_1, chin_service_1) {
    "use strict";
    var App = (function () {
        function App(api, chin) {
            this.api = api;
        }
        App.prototype.configureRouter = function (config, router) {
            config.title = 'Contacts.';
            config.map([
                {
                    route: '',
                    moduleId: 'boardlist',
                    name: 'boardlist',
                    title: 'Board list'
                },
                {
                    route: 'contacts/:id',
                    moduleId: 'contact-detail',
                    name: 'contacts'
                }
            ]);
            this.router = router;
        };
        App = __decorate([
            aurelia_framework_1.inject(web_api_1.WebAPI, chin_service_1.ChinService), 
            __metadata('design:paramtypes', [web_api_1.WebAPI, chin_service_1.ChinService])
        ], App);
        return App;
    }());
    exports.App = App;
});

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
define('board-list',["require", "exports", 'aurelia-framework', 'aurelia-event-aggregator', './netw/chin-service'], function (require, exports, aurelia_framework_1, aurelia_event_aggregator_1, chin_service_1) {
    "use strict";
    var BoardList = (function () {
        function BoardList(chin, ea) {
            this.chin = chin;
        }
        BoardList.prototype.created = function () {
            this.chin.getBoards()
                .then(function (boards) {
                console.log(boards);
            });
        };
        BoardList = __decorate([
            aurelia_framework_1.inject(chin_service_1.ChinService, aurelia_event_aggregator_1.EventAggregator), 
            __metadata('design:paramtypes', [chin_service_1.ChinService, aurelia_event_aggregator_1.EventAggregator])
        ], BoardList);
        return BoardList;
    }());
    exports.BoardList = BoardList;
});

define('utility',["require", "exports"], function (require, exports) {
    "use strict";
    function areEqual(obj1, obj2) {
        return Object.keys(obj1).every(function (key) { return obj2.hasOwnProperty(key) && (obj1[key] === obj2[key]); });
    }
    exports.areEqual = areEqual;
    ;
});

define('messages',["require", "exports"], function (require, exports) {
    "use strict";
    var ContactUpdated = (function () {
        function ContactUpdated(contact) {
            this.contact = contact;
        }
        return ContactUpdated;
    }());
    exports.ContactUpdated = ContactUpdated;
    var ContactViewed = (function () {
        function ContactViewed(contact) {
            this.contact = contact;
        }
        return ContactViewed;
    }());
    exports.ContactViewed = ContactViewed;
});

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
define('contact-detail',["require", "exports", 'aurelia-framework', './web-api', './utility', 'aurelia-event-aggregator', './messages'], function (require, exports, aurelia_framework_1, web_api_1, utility_1, aurelia_event_aggregator_1, messages_1) {
    "use strict";
    var ContactDetail = (function () {
        function ContactDetail(api, ea) {
            this.api = api;
            this.ea = ea;
        }
        ContactDetail.prototype.activate = function (params, routeConfig) {
            var _this = this;
            this.routeConfig = routeConfig;
            return this.api.getContactDetails(params.id).then(function (contact) {
                _this.contact = contact;
                _this.routeConfig.navModel.setTitle(_this.contact.firstName);
                _this.originalContact = JSON.parse(JSON.stringify(_this.contact));
                _this.ea.publish(new messages_1.ContactViewed(_this.contact));
            });
        };
        Object.defineProperty(ContactDetail.prototype, "canSave", {
            get: function () {
                return this.contact.firstName && this.contact.lastName && !this.api.isRequesting;
            },
            enumerable: true,
            configurable: true
        });
        ContactDetail.prototype.save = function () {
            var _this = this;
            this.api.saveContact(this.contact).then(function (contact) {
                _this.contact = contact;
                _this.routeConfig.navModel.setTitle(_this.contact.firstName);
                _this.originalContact = JSON.parse(JSON.stringify(_this.contact));
                _this.ea.publish(new messages_1.ContactUpdated(_this.contact));
            });
        };
        ContactDetail.prototype.canDeactivate = function () {
            if (!utility_1.areEqual(this.originalContact, this.contact)) {
                var result = confirm('You have unsaved change. Are you sure you wanna leave?');
                if (!result) {
                    this.ea.publish(new messages_1.ContactViewed(this.contact));
                }
                return result;
            }
            return true;
        };
        ContactDetail = __decorate([
            aurelia_framework_1.inject(web_api_1.WebAPI, aurelia_event_aggregator_1.EventAggregator), 
            __metadata('design:paramtypes', [web_api_1.WebAPI, aurelia_event_aggregator_1.EventAggregator])
        ], ContactDetail);
        return ContactDetail;
    }());
    exports.ContactDetail = ContactDetail;
});

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
define('contact-list',["require", "exports", './web-api', 'aurelia-framework', 'aurelia-event-aggregator', './messages'], function (require, exports, web_api_1, aurelia_framework_1, aurelia_event_aggregator_1, messages_1) {
    "use strict";
    var ContactList = (function () {
        function ContactList(api, ea) {
            var _this = this;
            this.api = api;
            this.selectedId = 0;
            ea.subscribe(messages_1.ContactViewed, function (msg) { return _this.select(msg.contact); });
            ea.subscribe(messages_1.ContactUpdated, function (msg) {
                var id = msg.contact.id;
                var found = _this.contacts.find(function (x) { return x.id == id; });
                Object.assign(found, msg.contact);
            });
        }
        ContactList.prototype.created = function () {
            var _this = this;
            this.api.getContactList().then(function (contacts) { return _this.contacts = contacts; });
        };
        ContactList.prototype.select = function (contact) {
            this.selectedId = contact.id;
            return true;
        };
        ContactList = __decorate([
            aurelia_framework_1.inject(web_api_1.WebAPI, aurelia_event_aggregator_1.EventAggregator), 
            __metadata('design:paramtypes', [web_api_1.WebAPI, aurelia_event_aggregator_1.EventAggregator])
        ], ContactList);
        return ContactList;
    }());
    exports.ContactList = ContactList;
});

define('environment',["require", "exports"], function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    exports.default = {
        debug: true,
        testing: true
    };
});

define('main',["require", "exports", './environment', 'whatwg-fetch'], function (require, exports, environment_1) {
    "use strict";
    Promise.config({
        longStackTraces: environment_1.default.debug,
        warnings: {
            wForgottenReturn: false
        }
    });
    function configure(aurelia) {
        aurelia.use
            .standardConfiguration()
            .developmentLogging()
            .feature('resources');
        if (environment_1.default.debug) {
            aurelia.use.developmentLogging();
        }
        if (environment_1.default.testing) {
            aurelia.use.plugin('aurelia-testing');
        }
        aurelia.start().then(function () { return aurelia.setRoot(); });
    }
    exports.configure = configure;
});

define('no-selection',["require", "exports"], function (require, exports) {
    "use strict";
    var NoSelection = (function () {
        function NoSelection() {
            this.message = "Please select a contact.";
        }
        return NoSelection;
    }());
    exports.NoSelection = NoSelection;
});

define('netw/serial',["require", "exports"], function (require, exports) {
    "use strict";
    var Serial = (function () {
        function Serial() {
        }
        Serial.prototype.fromJSON = function (json) {
            var jsonObject = JSON.parse(json);
            for (var propertyName in jsonObject) {
                this[propertyName] = jsonObject[propertyName];
            }
        };
        return Serial;
    }());
    exports.Serial = Serial;
});

define('netw/models',["require", "exports"], function (require, exports) {
    "use strict";
    var Boards = (function () {
        function Boards(boards) {
            this.boards = boards;
        }
        Boards.prototype.getBoards = function () {
            return this.boards;
        };
        return Boards;
    }());
    exports.Boards = Boards;
    var Board = (function () {
        function Board(board, title, meta_description) {
            this.board = board;
            this.title = title;
            this.meta_description = meta_description;
        }
        Board.prototype.getBoard = function () {
            return this.board;
        };
        Board.prototype.getTitle = function () {
            return this.title;
        };
        Board.prototype.getMetaDescription = function () {
            return this.meta_description;
        };
        return Board;
    }());
    exports.Board = Board;
    var ThreadList = (function () {
        function ThreadList() {
        }
        ThreadList.prototype.getPage = function () {
            return this.page;
        };
        ThreadList.prototype.getThreads = function () {
            return this.threads;
        };
        return ThreadList;
    }());
    exports.ThreadList = ThreadList;
    var ThreadSmall = (function () {
        function ThreadSmall() {
        }
        ThreadSmall.prototype.getNo = function () {
            return this.no;
        };
        ThreadSmall.prototype.getLastModified = function () {
            return this.last_modified;
        };
        return ThreadSmall;
    }());
    exports.ThreadSmall = ThreadSmall;
    var ThreadPage = (function () {
        function ThreadPage() {
        }
        ThreadPage.prototype.getPage = function () {
            return this.page;
        };
        ThreadPage.prototype.getThreads = function () {
            return this.threads;
        };
        return ThreadPage;
    }());
    exports.ThreadPage = ThreadPage;
    var Post = (function () {
        function Post() {
        }
        Post.prototype.getNo = function () {
            return this.no;
        };
        Post.prototype.getNow = function () {
            return this.now;
        };
        Post.prototype.getName = function () {
            return this.name;
        };
        Post.prototype.getCom = function () {
            return this.com;
        };
        Post.prototype.getTime = function () {
            return this.time;
        };
        return Post;
    }());
    exports.Post = Post;
});

define('resources/index',["require", "exports"], function (require, exports) {
    "use strict";
    function configure(config) {
    }
    exports.configure = configure;
});



define("resources/elements/loading-indicator", [],function(){});

define('text!app.html', ['module'], function(module) { module.exports = "<template>\r\n  <require from=\"bootstrap/css/bootstrap.css\"></require>\r\n  <require from=\"./styles.css\"></require>\r\n  <require from=\"./board-list\"></require>\r\n\r\n  <nav class=\"navbar navbar-default navbar-fixed-top\" role=\"navigation\">\r\n    <div class=\"navbar-header\">\r\n      <a href=\"#\" class=\"navbar-brand\">\r\n        <i class=\"fa fa-user\"></i>\r\n        <span>Eek.</span>\r\n      </a>\r\n    </div>\r\n  </nav>\r\n  <div class=\"container\">\r\n    <div class=\"row\">\r\n      <board-list class=\"col-md-12\"></boardlist>\r\n    </div>\r\n  </div>\r\n</template>\r\n"; });
define('text!styles.css', ['module'], function(module) { module.exports = "body { padding-top: 70px; }\r\n\r\nsection {\r\n  margin: 0 20px;\r\n}\r\n\r\na:focus {\r\n  outline: none;\r\n}\r\n\r\n.navbar-nav li.loader {\r\n    margin: 12px 24px 0 6px;\r\n}\r\n\r\n.no-selection {\r\n  margin: 20px;\r\n}\r\n\r\n.contact-list {\r\n  overflow-y: auto;\r\n  border: 1px solid #ddd;\r\n  padding: 10px;\r\n}\r\n\r\n.panel {\r\n  margin: 20px;\r\n}\r\n\r\n.button-bar {\r\n  right: 0;\r\n  left: 0;\r\n  bottom: 0;\r\n  border-top: 1px solid #ddd;\r\n  background: white;\r\n}\r\n\r\n.button-bar > button {\r\n  float: right;\r\n  margin: 20px;\r\n}\r\n\r\nli.list-group-item {\r\n  list-style: none;\r\n}\r\n\r\nli.list-group-item > a {\r\n  text-decoration: none;\r\n}\r\n\r\nli.list-group-item.active > a {\r\n  color: white;\r\n}\r\n"; });
define('text!board-list.html', ['module'], function(module) { module.exports = "<template>\r\n    <div class=\"board-list\">\r\n        <ul class=\"list-group\">\r\n            <li repeat.for=\"board of boards\" class=\"list-group-item\">\r\n                <h4 class=\"list-group-item-heading\">${board.board} ${board.title}</h4>\r\n                <p class=\"list-group-item-text\">${board.meta_description}</p>\r\n            </li>\r\n        </ul>\r\n    </div>\r\n</template"; });
define('text!contact-detail.html', ['module'], function(module) { module.exports = "<template>\r\n    <div class=\"panel panel-primary\">\r\n        <div class=\"panel-heading\">\r\n            <h3 class=\"panel-title\">Profile</h3>\r\n        </div>\r\n        <div class=\"panel-body\">\r\n            <form role=\"form\" class=\"form-horizontal\">\r\n                <div class=\"form-group\">\r\n                    <label class=\"col-sm-2 control-label\">First Name</label>\r\n                    <div class=\"col-sm-10\">\r\n                        <input type=\"text\" placeholder=\"first name\" class=\"form-control\" value.bind=\"contact.firstName\">\r\n                    </div>\r\n                </div>\r\n\r\n                <div class=\"form-group\">\r\n                    <label class=\"col-sm-2 control-label\">Last Name</label>\r\n                    <div class=\"col-sm-10\">\r\n                        <input type=\"text\" placeholder=\"last name\" class=\"form-control\" value.bind=\"contact.lastName\">\r\n                    </div>\r\n                </div>\r\n\r\n                <div class=\"form-group\">\r\n                    <label class=\"col-sm-2 control-label\">Email</label>\r\n                    <div class=\"col-sm-10\">\r\n                        <input type=\"text\" placeholder=\"email\" class=\"form-control\" value.bind=\"contact.email\">\r\n                    </div>\r\n                </div>\r\n\r\n                <div class=\"form-group\">\r\n                    <label class=\"col-sm-2 control-label\">Phone Number</label>\r\n                    <div class=\"col-sm-10\">\r\n                        <input type=\"text\" placeholder=\"phone number\" class=\"form-control\" value.bind=\"contact.phoneNumber\">\r\n                    </div>\r\n                </div>\r\n            </form>\r\n        </div>\r\n    </div>\r\n\r\n    <div class=\"button-bar\">\r\n        <button class=\"btn btn-success\" click.delegate=\"save()\" disabled.bind=\"!canSave\">Save</button>\r\n    </div>\r\n</template>"; });
define('text!contact-list.html', ['module'], function(module) { module.exports = "<template>\r\n    <div class=\"contact-list\">\r\n        <ul class=\"list-group\">\r\n            <li repeat.for=\"contact of contacts\" class=\"list-group-item ${contact.id === $parent.selectedId ? 'active' : ''}\">\r\n                <a route-href=\"route: contacts; params.bind: {id: contact.id}\" click.delegate=\"$parent.select(contact)\">\r\n                    <h4 class=\"list-group-item-heading\">${contact.firstName} ${contact.lastName}</h4>\r\n                    <p class=\"list-group-item-text\">${contact.item}</p>\r\n                </a>\r\n            </li>\r\n        </ul>\r\n    </div>\r\n</template>"; });
define('text!no-selection.html', ['module'], function(module) { module.exports = "<template>\r\n    <div class=\"no-selection text-center\">\r\n        <h2>${message}</h2>\r\n    </div>\r\n</template>"; });
//# sourceMappingURL=app-bundle.js.map
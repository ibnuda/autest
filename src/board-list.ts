import {inject} from 'aurelia-framework';
import {EventAggregator} from 'aurelia-event-aggregator';
import {ChinService} from './netw/chin-service';

@inject(ChinService, EventAggregator)
export class BoardList {
    boards;

    constructor(private chin: ChinService, ea: EventAggregator) {
        /*
        this.chin.getBoards();
        this.chin.getBoards()
            .then(boards => this.boards = boards.boards)
            .catch(wut => console.log(wut));
        */
    }

    created() {
        this.chin.getBoards()
            .then(boards => {
                console.log(boards);
                /*
                let jaran = JSON.parse(boards);
                this.boards = jaran;
                */
            });
    }
}
export class Serial {
    fromJSON(json: string) {
        var jsonObject = JSON.parse(json);
        for (var propertyName in jsonObject) {
            this[propertyName] = jsonObject[propertyName];
        }
    }
}
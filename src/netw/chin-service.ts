import {HttpClient} from 'aurelia-http-client';
// import { HttpClient } from 'aurelia-fetch-client';
import { inject } from 'aurelia-framework';
import 'whatwg-fetch';

@inject(HttpClient)
export class ChinService {
    public http: HttpClient;
    boards;

    constructor(http: HttpClient) {
        this.http = http.configure(config => {
            config
                .withBaseUrl('http://a.4cdn.org/')
                /*
                .withDefaults({
                    headers: {
                        'Access-Control-Request-Method': 'GET',
                        'Accept': 'application/json',
                        'X-Requested-With': 'Fetch'
                    }
                })
                */
                // .withHeader('Access-Control-Request-Method', 'GET')
                // .withHeader('Access-Control-Allow-Origin', '*')
                // .withHeader('Access-Control-Allow-Headers', '*')
                .withInterceptor({
                    request(req) {
                        console.log(`requesting method: ${req.method}, url: ${req.url}`);
                        return req;
                    },
                    response(resp) {
                        console.log(`response status: ${resp.statusCode}, content : ${resp.content}`);
                        return resp;
                    }
                });
        });
    }

    getBoards() {
        console.log("what the fuck?!");
        return this.http
            .get(`boards.json`) // , {'mode': 'no-cors'})
                .then(resp => {
                    if (resp.headers.get('Content-Type') === 'application/json') {
                        console.log("it looks like this is a json file.");
                        return resp.response;
                    }
                    console.log("it looks like this is not a json file.");
                    // console.log(resp.type);
                    // return resp.text();
                    return resp.responseType;
                });
                    /*
            .fetch(`boards.json`, { 'mode': 'no-cors' })
            .then(resp => resp.json()
                .then(data => ({
                    data: data,
                    status: resp.status
                }))
                .then(resp => {
                    console.log(resp.data.boards, resp.status);
                    if (resp.headers.get('Content-Type') === 'application/json') {
                        return resp.json();
                    }
                    return resp.text();
                }));
                    */
    }
}
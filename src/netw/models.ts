import {Serial} from './serial';

export class Boards {
    boards: Board [];

    constructor(boards: Board[]) {
        this.boards = boards;
    }

    getBoards() {
        return this.boards;
    }
}

export class Board {
    board: string;
    title: string;
    meta_description: string;

    constructor(board: string, title: string, meta_description: string) {
        this.board = board;
        this.title = title;
        this.meta_description = meta_description;
    }

    getBoard() {
        return this.board;
    }
    getTitle() {
        return this.title;
    }
    getMetaDescription() {
        return this.meta_description;
    }
}

export class ThreadList {
    page: number;
    threads: ThreadSmall [];

    getPage() {
        return this.page;
    }
    getThreads() {
        return this.threads;
    }
}

export class ThreadSmall {
    no: number;
    last_modified: number;

    getNo() {
        return this.no;
    }
    getLastModified() {
        return this.last_modified;
    }
}

export class ThreadPage {
    page: number;
    threads: Post [];

    getPage() {
        return this.page;
    }
    getThreads() {
        return this.threads;
    }
}

export class Post {
    no: number;
    now: string;
    name: string;
    com: string;
    time: number;

    getNo() {
        return this.no;
    }
    getNow() {
        return this.now;
    }
    getName() {
        return this.name;
    }
    getCom() {
        return this.com;
    }
    getTime() {
        return this.time;
    }
}

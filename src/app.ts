import {Router, RouterConfiguration} from 'aurelia-router';
import {inject} from 'aurelia-framework';
import {WebAPI} from './web-api';
import {ChinService} from './netw/chin-service';

@inject(WebAPI, ChinService)
export class App {
  router: Router;

  constructor(public api: WebAPI, chin: ChinService) {}

  configureRouter(config: RouterConfiguration, router: Router) {
    config.title = 'Contacts.';
    config.map([
      /*
      {
        route: '',
        moduleId: 'no-selection',
        title: 'Select'
      },
      */
      {
        route: '',
        moduleId: 'boardlist',
        name: 'boardlist',
        title: 'Board list'
      },
      {
        route: 'contacts/:id',
        moduleId: 'contact-detail',
        name: 'contacts'
      }
    ]);
    this.router = router;
  }
}
